<?php
    namespace Tools;

    class Tools {
        public static function redirectTo($url) {
            header("Location: " . Settings::$baseURL . $url);
        }
    }
?>