<?php
    namespace Tools;
    
    class Convert {
        public static function ArrayToJSON(array $array) : string {
            return json_encode($array, JSON_PRETTY_PRINT);
        }
        
        public static function JSONToArray(string $json) : array {
            return json_decode($json, true);
        }

        public static function JSONToClass(string $json) : object {
            return json_decode($json);
        }

        public static function toBoolean(string $value) : bool {
            return ($value === 1 || $value === "1") ? true : false;
        }

        public static function toLowerCase(array $array) : array {
            return array_change_key_case($array, CASE_LOWER);
        }
    }
?>