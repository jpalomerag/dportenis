<?php
    namespace Tools;

    class Settings {
        public static $baseURL = 'http://localhost';

        public static $routes = [
            "/",
            "/ejercicio/1",
            "/ejercicio/2",
            "/ejercicio/3",

            "/articulos",
            "/articulos/actualizar",
            "/articulos/registrar",
            "/articulos/eliminar",

            "/dividir",
            "/factorial",

            "/404",
            "/405"
        ];
    }
?>