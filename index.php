<?php
    require_once "vendor/autoload.php";

    use Controllers\RouteController;

    $route = new RouteController();
    $route->init();
?>