<?php
    namespace Models;

    use PDO;
    use Tools\Convert;
    use Tools\DataBase;

    class BaseModel extends DataBase {
        private $table;

        function __construct($table)
        {
            $this->table = $table;
        }

        public function getAll(string $nombreRegistros = "registros", string $campos = '*', int $activos = 1) {
            $conexion = parent::Conectar();

            $consulta = $conexion->prepare('SELECT '. $campos .' FROM '. $this->table .' WHERE ACTIVO = COALESCE(1, COALESCE(:activo, 0)) AND BORRADO = 0');
            $consulta->bindValue(':activo', $activos);
            $consulta->execute();
            $registros = $consulta->fetchAll(PDO::FETCH_ASSOC);

            if ($registros) {
                for($i=0;$i<count($registros);$i++) {
                    if (array_key_exists('ACTIVO', $registros[$i]))
                        $registros[$i]['ACTIVO'] = Convert::toBoolean($registros[$i]['ACTIVO']);
                    
                    $registros[$i] = Convert::toLowerCase($registros[$i]);
                }

                $resultado = true;
                $mensaje = $nombreRegistros . " encontrados.";
            } else {
                $resultado = false;
                $mensaje = $nombreRegistros . " no encontrado(s).";
            }
            $conexion = null;

            return array(
                'resultado'         => $resultado,
                'mensaje'           => $mensaje,
                $nombreRegistros    => $registros
            );
        }

        public function delete(string $codigo, $nombreRegistro = 'Registro') {
            if (isset($codigo) && $codigo !== '') {
                $conexion = parent::Conectar();

                $consulta = $conexion->prepare('UPDATE '. $this->table .' SET BORRADO = 1 WHERE CODIGO LIKE :codigo');
                $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
                $consulta->execute();

                $conexion = null;

                return array(
                    'resultado' => true,
                    'mensaje'   => 'El '. $nombreRegistro .' ha sido eliminado.'
                );
            } else {
                return array(
                    'resultado' => false,
                    'mensaje'   => 'Código no especificado.'
                );
            }
        }
    }
?>