<?php
    namespace Models;

use PDO;
use Tools\Convert;

class ArticuloModel extends BaseModel {
        private $table;

        function __construct()
        {
            $this->table = 'DP_ARTICULOS';
            parent::__construct($this->table);
        }

        public function register(string $codigo, string $articulo, float $precio, int $activo) {
            $conexion = parent::Conectar();

            $consulta = $conexion->prepare('SELECT 1 FROM '. $this->table .' WHERE CODIGO LIKE UPPER(:codigo)');
            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
            $consulta->execute();

            $registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
            if ($registros) {
                $conexion = null;

                return array(
                    'resultado' => false,
                    'mensaje'   => 'El codigo ya se encuentra utilizado.'
                );
            } else {
                $consulta = $conexion->prepare('INSERT INTO '. $this->table .'(CODIGO, ARTICULO, PRECIO, ACTIVO) VALUES(UPPER(:codigo), UPPER(:articulo), :precio, :activo)');
                $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
                $consulta->bindValue(':articulo', $articulo, PDO::PARAM_STR);
                $consulta->bindValue(':precio', $precio);
                $consulta->bindValue(':activo', $activo, PDO::PARAM_BOOL);
                $consulta->execute();

                $conexion = null;

                return array(
                    'registros' => $registros,
                    'resultado' => true,
                    'mensaje'   => 'El artículo ha sido registrado.'
                );
            }
        }

        public function update(string $codigo, string $articulo, float $precio, int $activo) {
            $conexion = parent::Conectar();

            $consulta = $conexion->prepare('UPDATE '. $this->table .' SET '
                .'ARTICULO = UPPER(:articulo)'
                .', PRECIO = :precio'
                .', ACTIVO = :activo'
            .' WHERE CODIGO LIKE :codigo');
            $consulta->bindValue(':articulo', isset($articulo) ? $articulo : '', PDO::PARAM_STR);
            $consulta->bindValue(':precio', isset($precio) ? $precio : 0.0);
            $consulta->bindValue(':activo', $activo, PDO::PARAM_BOOL);
            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
            $consulta->execute();

            $conexion = null;

            return array(
                'resultado' => true,
                'mensaje'   => 'El artículo ha sido actualizado.'
            );
        }

        public function getAllWithIVA(string $clave = "%", string $articulo="%", int $activos = 1) {
            $conexion = parent::Conectar();
            $consulta = $conexion->prepare('SELECT ID_ARTICULO, CODIGO, ARTICULO, PRECIO
                , ROUND(PRECIO * COALESCE((SELECT PORCENTAJE_IVA / 100 FROM DP_CONFIGURACIONES WHERE FECHA_INICIA_PERIODO <= NOW() AND (FECHA_TERMINA_PERIODO >= NOW() OR FECHA_TERMINA_PERIODO IS NULL) AND ACTIVO = 1 AND BORRADO = 0 LIMIT 1), 0), 2) AS IVA
                , ROUND(PRECIO + PRECIO * COALESCE((SELECT PORCENTAJE_IVA / 100 FROM DP_CONFIGURACIONES WHERE FECHA_INICIA_PERIODO <= NOW() AND (FECHA_TERMINA_PERIODO >= NOW() OR FECHA_TERMINA_PERIODO IS NULL) AND ACTIVO = 1 AND BORRADO = 0 LIMIT 1), 0), 2) AS PRECIO_NETO
                , ACTIVO FROM '. $this->table 
            .' WHERE CODIGO LIKE :clave'
                .' AND ARTICULO LIKE :articulo'
                .' AND ACTIVO IN(1, COALESCE(:activo, 1))'
                .' AND BORRADO = 0');
            $consulta->bindValue(':clave', isset($clave) ? $clave : '%', PDO::PARAM_STR);
            $consulta->bindValue(':articulo', isset($articulo) ? $articulo : '%', PDO::PARAM_STR);
            $consulta->bindValue(':activo', $activos, PDO::PARAM_BOOL);
            $consulta->execute();

            $registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
            if ($registros) {
                for($i=0;$i<count($registros);$i++) {
                    $registros[$i]['ACTIVO'] = Convert::toBoolean($registros[$i]['ACTIVO']);
                    $registros[$i] = Convert::toLowerCase($registros[$i]);
                }

                $resultado = true;
                $mensaje = "Artículos encontrados.";
            } else {
                $resultado = false;
                $mensaje = "Artículos no encontrado(s).";
            }
            $conexion = null;

            return array(
                'resultado'    => $resultado,
                'mensaje'      => $mensaje,
                'articulos'    => $registros
            );
        }
    }
?>