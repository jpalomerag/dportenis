<!DOCTYPE html>
<html lang="mx">
    <head>
        <meta charset="utf-8" />
        <title>Bienvenido</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="../assets/jquery.toast.min.css" rel="stylesheet">

        <script src="https://unpkg.com/vue@3"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="../assets/jquery.toast.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <style>
            .hand {
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="app-crud container<?php echo (isset($full))?'-fluid':''; ?>">
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-9 col-xs-7 col-sm-9 " style="padding-left:25px;">
                    <h3>Bienvenido</h3>
                </div>
                </div>
            </div>
            <hr class="divider-title" style="margin-top: 10px;">
            <section class="cuerpo">
                <div class="row">
                <div class="col-md-12">
                    <div style="height:472px" class="panel panel-default">

                    <div class="row">
                        <div class="col-2">
                            <ul class="list-group list-group-flush">
                                <a href="/ejercicio/1" class="hand list-group-item">Ejercicio 1</a>
                                <a href="/ejercicio/2" class="hand list-group-item">Ejercicio 2</a>
                                <a href="/ejercicio/3" class="hand list-group-item">Ejercicio 3</a>
                            </ul>
                        </div>
                    </div>

                    </div>
                </div>
                </div>
            </section>
        </div>
        <script src="../assets/app.js"></script>
    </body>
</html>