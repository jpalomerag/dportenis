<!DOCTYPE html>
<html lang="mx">
    <head>
        <meta charset="utf-8" />
        <title>EjercicioWeb 3</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="../assets/jquery.toast.min.css" rel="stylesheet">

        <script src="https://unpkg.com/vue@3"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="../assets/jquery.toast.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <style>
            .hand {
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="app-factorial container<?php echo (isset($full))?'-fluid':''; ?>">
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-9 col-xs-7 col-sm-9 " style="padding-left:25px;">
                    <h3>Ejercicio #3</h3>
                </div>
                </div>
            </div>
            <hr class="divider-title" style="margin-top: 10px;">
            <section class="cuerpo">
                <div class="row">
                    <div class="col-md-12">
                        <div style="height:472px" class="panel panel-default">

                            <div class="row">
                                <div class="col-3">
                                    <label for="lblValor" class="form-label">Valor</label>
                                    <input type="text" maxlength="3" class="form-control" id="lblValor" v-model="valor" v-on:keyup="onKeyUpFactorial()" placeholder="Ej. 10">
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-3">
                                    <label for="lblResultado" class="form-label">Resultado Factorial</label>
                                    <input type="number" class="form-control" id="lblResultado" v-model="factorial" disabled>
                                </div>
                            </div>

                            <div class="row mt-1">
                                <div class="col-10">
                                    <hr/>
                                    <label v-if="factorial" class="ms-1" v-for="(valor, index) in getFactorialExpresion()" :key="index">
                                        {{valor}} <label v-if="index != getFactorialExpresion().length - 1">x</label>
                                    </label>
                                    <label v-if="mensaje!=''" class="text-danger">{{mensaje}}</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script src="../assets/app.js"></script>
        <script type="text/javascript">
            $("#lblValor").keyup(function() {
                $("#lblValor").val(this.value.match(/[0-9]*/));
            });
        </script>
    </body>
</html>