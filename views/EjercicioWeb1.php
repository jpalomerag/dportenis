<!DOCTYPE html>
<html lang="mx">
    <head>
        <meta charset="utf-8" />
        <title>EjercicioWeb 1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="../assets/jquery.toast.min.css" rel="stylesheet">

        <script src="https://unpkg.com/vue@3"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="../assets/jquery.toast.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <style>
            .hand {
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="app-crud container<?php echo (isset($full))?'-fluid':''; ?>">
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-9 col-xs-7 col-sm-9 " style="padding-left:25px;">
                    <h3>Ejercicio #1</h3>
                </div>
                </div>
            </div>
            <hr class="divider-title" style="margin-top: 10px;">
            <section class="cuerpo">
                <div class="row">
                <div class="col-md-12">
                    <div style="height:472px" class="panel panel-default">

                        <form>
                            <div class="row">
                                <div class="col-3">
                                    <label for="lblClave" class="form-label">Clave</label>
                                    <input type="text" class="form-control" id="lblClave" v-model="buscarClave" placeholder="Ej. 00000" v-on:keyup="onKeyUpBuscar($event)">
                                </div>
                                <div class="col-9">
                                    <label for="lblArticulo" class="form-label">Artículo</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="lblArticulo" v-model="buscarArticulo" placeholder="Ej. PUMAS F0KIS9" v-on:keyup="onKeyUpBuscar($event)">
                                        <span class="input-group-text hand" id="btnBuscar" v-on:click="onClickBuscar">Buscar</span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row mt-3">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <thead>
                                        <th class="text-center" colspan="2"><i class="hand fas fa-plus" v-on:click="onClickNuevoRegistro"></i></th>
                                        <th class="text-center col-2">Clave</th>
                                        <th class="text-center col-4">Artículo</th>
                                        <th class="text-center col-2">Precio</th>
                                        <th class="text-center col-2">IVA</th>
                                        <th class="text-center col-2">Precio Neto</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td v-if="nuevoArticulo.isEditing" colspan="7">
                                                <form>
                                                    <div class="row ms-5">
                                                        <div class="col-2">
                                                            <label for="lblCodigo" class="form-label">Código</label>
                                                            <input type="email" class="form-control" id="lblCodigo" v-model="nuevoArticulo.codigo" placeholder="Ej. 00000">
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="lblArticulo" class="form-label">Artículo</label>
                                                            <input type="email" class="form-control" id="lblArticulo" v-model="nuevoArticulo.articulo" placeholder="Ej. NIKE VK919">
                                                        </div>
                                                        <div class="col-2">
                                                            <label for="lblPrecio" class="form-label">Precio</label>
                                                            <input type="email" class="form-control" id="lblPrecio" v-model="nuevoArticulo.precio" placeholder="Ej. 10.00">
                                                        </div>
                                                        <div class="col-1">
                                                            <div class="form-check form-switch mt-4">
                                                                <input class="form-check-input" type="checkbox" id="inActivo" v-model="nuevoArticulo.activo">
                                                                <label class="form-check-label" for="inActivo">Activo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row ms-5 mt-2">
                                                        <div class="col-12">
                                                            <a id="btnGuardar" class="btn btn-primary me-3" v-on:click="onClickRegistrar(nuevoArticulo)">Guardar</a>    
                                                            <a id="btnCancelar" class="btn btn-secondary" v-on:click="onClickCancelar(nuevoArticulo)">Cancelar</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>

                                        <tr v-for="(articulo, index) in listaArticulos" :key="index">
                                            <td v-if="!articulo.isEditing" class="text-center"><i class="hand fas fa-pencil-alt" v-on:click="onClickEditar(articulo)"></i></td>
                                            <td v-if="!articulo.isEditing" class="text-center"><i class="hand fas fa-trash-alt" v-on:click="onClickEliminar(articulo)"></i></td>
                                            <td v-if="!articulo.isEditing" class="text-center">{{articulo.codigo}}</td>
                                            <td v-if="!articulo.isEditing" class="text-center">{{articulo.articulo}}</td>
                                            <td v-if="!articulo.isEditing" class="text-center">{{articulo.precio}}</td>
                                            <td v-if="!articulo.isEditing" class="text-center">{{articulo.iva}}</td>
                                            <td v-if="!articulo.isEditing" class="text-center">{{articulo.precio_neto}}</td>

                                            <td v-if="articulo.isEditing" colspan="7">
                                                <form>
                                                    <div class="row ms-5">
                                                        <div class="col-2">
                                                            <label for="lblCodigo" class="form-label">Código</label>
                                                            <input type="email" class="form-control" id="lblCodigo" v-model="articulo.codigo" placeholder="Ej. 00000" disabled>
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="lblArticulo" class="form-label">Artículo</label>
                                                            <input type="email" class="form-control" id="lblArticulo" v-model="articulo.articulo" placeholder="Ej. NIKE VK919">
                                                        </div>
                                                        <div class="col-2">
                                                            <label for="lblPrecio" class="form-label">Precio</label>
                                                            <input type="email" class="form-control" id="lblPrecio" v-model="articulo.precio" placeholder="Ej. 10.00">
                                                        </div>
                                                        <div class="col-1">
                                                            <div class="form-check form-switch mt-4">
                                                                <input class="form-check-input" type="checkbox" id="inActivo" v-model="articulo.activo">
                                                                <label class="form-check-label" for="inActivo">Activo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row ms-5 mt-2">
                                                        <div class="col-12">
                                                            <a id="btnGuardar" class="btn btn-primary me-3" v-on:click="onClickActualizar(articulo)">Guardar</a>    
                                                            <a id="btnCancelar" class="btn btn-secondary" v-on:click="onClickCancelar(articulo)">Cancelar</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
            </section>
        </div>
        <script src="../assets/app.js"></script>
    </body>
</html>