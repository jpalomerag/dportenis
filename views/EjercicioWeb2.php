<!DOCTYPE html>
<html lang="mx">
    <head>
        <meta charset="utf-8" />
        <title>EjercicioWeb 2</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="../assets/jquery.toast.min.css" rel="stylesheet">

        <script src="https://unpkg.com/vue@3"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="../assets/jquery.toast.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <style>
            .hand {
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="app-division container<?php echo (isset($full))?'-fluid':''; ?>">
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-9 col-xs-7 col-sm-9 " style="padding-left:25px;">
                    <h3>Ejercicio #2</h3>
                </div>
                </div>
            </div>
            <hr class="divider-title" style="margin-top: 10px;">
            <section class="cuerpo">
                <div class="row">
                    <div class="col-md-12">
                        <div style="height:472px" class="panel panel-default">

                            <div class="row">
                                <div class="col-4">
                                    <label for="lblPrimerValor" class="form-label">Primer Valor</label>
                                    <input type="number" class="form-control" id="lblPrimerValor" v-model="primerValor" min="0" placeholder="Ej. 10">
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-4">
                                    <label for="lblSegundoValor" class="form-label">Segundo Valor</label>
                                    <input type="number" class="form-control" id="lblSegundoValor" v-model="segundoValor" min="0" placeholder="Ej. 2">
                                </div>
                            </div>
                            
                            <div class="col-4 mt-3 pe-3">
                                <a id="btnCalcular" class="btn btn-primary col-12" v-on:click="onClickCalcular()">Calcular</a>
                            </div>
                        
                            <div class="row mt-2">
                                <div class="col-2">
                                    <label for="lblRespuesta" class="form-label">Resultado</label>
                                    <input type="text" class="form-control" id="lblRespuesta" v-model="respuesta" disabled>
                                </div>
                                <div class="col-2">
                                    <label for="lblResiduo" class="form-label">Residuo</label>
                                    <input type="text" class="form-control" id="lblResiduo" v-model="residuo" disabled>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4">
                                    <hr />
                                    <label v-bind:class="esDivisible ? 'text-success' : 'text-danger'">
                                        {{mensaje}}
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script src="../assets/app.js"></script>
    </body>
</html>