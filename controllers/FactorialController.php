<?php
    namespace Controllers;

    use Tools\Convert;

    class FactorialController extends BaseController {
        function __construct()
        {
            parent::__construct();
        }

        public static function CalcularFactorial($body) {
            if (isset($body['valor'])) {
                if ($body['valor'] != "") {
                    echo Convert::ArrayToJSON(array(
                        'resultado' => true,
                        'factorial' => gmp_strval(gmp_fact((int)$body['valor']))
                    ));
                } else {
                    echo Convert::ArrayToJSON(array(
                        'resultado' => false,
                        'mensaje' => 'El valor no ha sido especificado.'
                    ));
                }
            } else {
                echo Convert::ArrayToJSON(array(
                    'resultado' => false,
                    'mensaje' => 'El valor no ha sido especificado.'
                ));
            }
        }
    }
?>