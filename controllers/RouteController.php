<?php
    namespace Controllers;

use Tools\Convert;
use Tools\Settings;
    use Tools\Tools;

    class RouteController {        
        public static function init() {
            $route = explode('?',$_SERVER['REQUEST_URI'],2)[0];
            $valido = false;

            foreach(Settings::$routes as $r) {
                if($route == $r)
                    $valido = true;
            }

            if ($valido)
                RouteController::Routes();
            else
                Tools::redirectTo('/404');

            die();
        }

        public static function getParams() {
            $url = parse_url($_SERVER['REQUEST_URI']);
            if (isset($url['query']))
                parse_str($url['query'], $params);
            else
                $params = array();

            return $params;
        }

        public static function get($route, $function) {
            $cleanRoute = explode('?',$_SERVER['REQUEST_URI'],2)[0];

            if ($cleanRoute == $route) {
                $method = $_SERVER['REQUEST_METHOD'];
                if ($method != 'GET') {
                    Tools::redirectTo('/405');
                    return;
                }
                $function->__invoke(RouteController::getParams());

                die();
            } else {return;}
        }

        public static function post($route, $function) {
            $cleanRoute = explode('?',$_SERVER['REQUEST_URI'],2)[0];
            
            if ($cleanRoute == $route) {
                $method = $_SERVER['REQUEST_METHOD'];
                if ($method != 'POST') {
                    header("HTTP/1.0 405 Method Not Allowed");
                    return;
                }

                $function->__invoke(RouteController::getBody());
                die();
            } else {return;}
        }

        public static function getBody() {
            $body = file_get_contents('php://input');
            return Convert::JSONToArray($body);
        }

        public static function Routes() {
            RouteController::get('/', function () {
                (new BaseController())->CreateView('main');
            });

            RouteController::get('/ejercicio/1', function () {
                ArticuloController::CreateView('EjercicioWeb1');
            });

            RouteController::get('/ejercicio/2', function () {
                DivisionController::CreateView('EjercicioWeb2');
            });

            RouteController::get('/ejercicio/3', function () {
                FactorialController::CreateView('EjercicioWeb3');
            });



            RouteController::get('/articulos', function ($params) {
                ArticuloController::ObtenerArticulos($params);
            });

            RouteController::post('/articulos/actualizar', function ($body) {
                ArticuloController::ActualizarArticulo($body);
            });

            RouteController::post('/articulos/registrar', function ($body) {
                ArticuloController::RegistrarArticulo($body);
            });

            RouteController::post('/articulos/eliminar', function ($body) {
                ArticuloController::EliminarArticulo($body);
            });           
            
            
            RouteController::post('/dividir', function ($body) {
                DivisionController::Dividir($body);
            });

            RouteController::post('/factorial', function ($body) {
                FactorialController::CalcularFactorial($body);
            });



            RouteController::get('/404', function () {
                (new BaseController())->CreateView('404');
            });
        }
    }
?>