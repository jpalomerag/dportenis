<?php
    namespace Controllers;

    class BaseController {

        function __construct()
        {
            
        }

        public static function CreateView($viewName, $urlParams = null) {
            require_once('./views/'.$viewName.'.php');
        }
    }
?>