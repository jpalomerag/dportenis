<?php
    namespace Controllers;

    use Models\ArticuloModel;
    use Tools\Convert;

    class ArticuloController extends BaseController {
        function __construct()
        {
            parent::__construct();
        }

        public static function ActualizarArticulo($body) {
            header('Content-Type: application/json');

            if (isset($body['articulo']['codigo']) && isset($body['articulo']['articulo']) && isset($body['articulo']['precio'])) {
                if ($body['articulo']['codigo'] !== "" && $body['articulo']['articulo'] !== "" && $body['articulo']['precio'] !== "") {
                    $codigo = $body['articulo']['codigo'];
                    $articulo = $body['articulo']['articulo'];
                    $precio = $body['articulo']['precio'];
                    $activo = isset($body['articulo']['activo']) ? $body['articulo']['activo'] : 0;

                    echo Convert::ArrayToJSON((new ArticuloModel())->update($codigo, $articulo, $precio, $activo));
                } else {
                    echo Convert::ArrayToJSON(array(
                        'resultado' => false,
                        'mensaje' => 'Parametros no especificados.'
                    ));
                }
            } else {
                echo Convert::ArrayToJSON(array(
                    'resultado' => false,
                    'mensaje' => 'Parametros no especificados.'
                ));
            }
        }

        public static function RegistrarArticulo($body) {
            header('Content-Type: application/json');

            if (isset($body['articulo']['codigo']) && isset($body['articulo']['articulo']) && isset($body['articulo']['precio'])) {
                if ($body['articulo']['codigo'] !== "" && $body['articulo']['articulo'] !== "" && $body['articulo']['precio'] !== "") {
                    $codigo = $body['articulo']['codigo'];
                    $articulo = $body['articulo']['articulo'];
                    $precio = $body['articulo']['precio'];
                    $activo = isset($body['articulo']['activo']) ? $body['articulo']['activo'] : 0;

                    echo Convert::ArrayToJSON((new ArticuloModel())->register($codigo, $articulo, $precio, $activo));
                } else {
                    echo Convert::ArrayToJSON(array(
                        'resultado' => false,
                        'mensaje' => 'Parametros no especificados.'
                    ));
                }
            } else {
                echo Convert::ArrayToJSON(array(
                    'resultado' => false,
                    'mensaje' => 'Parametros no especificados.'
                ));
            }
        }

        public static function ObtenerArticulos($params) {
            header('Content-Type: application/json');

            $clave = isset($params['clave']) ? '%'.$params['clave'].'%' : '%';
            $articulo = isset($params['articulo']) ? '%'.$params['articulo'].'%' : '%';

            echo Convert::ArrayToJSON((new ArticuloModel())->getAllWithIVA($clave, $articulo, 0));
        }

        public static function EliminarArticulo($body) {
            header('Content-Type: application/json');

            $codigo = isset($body['articulo']['codigo']) ? $body['articulo']['codigo'] : '';

            echo Convert::ArrayToJSON((new ArticuloModel())->delete($codigo, 'Artículo'));
        }
    }
?>