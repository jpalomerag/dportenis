<?php
    namespace Controllers;

    use Tools\Convert;

    class DivisionController extends BaseController {
        function __construct()
        {
            parent::__construct();
        }

        public static function Dividir($body) {
            header('Content-Type: application/json');

            if (isset($body['primerValor']) && isset($body['segundoValor'])) {
                if ($body['primerValor'] != "" && $body['segundoValor'] != "") {
                    $primerValor = (float) $body['primerValor'];
                    $segundoValor = (float) $body['segundoValor'];

                    if ($segundoValor != 0) {
                        $resultado = $primerValor / $segundoValor;

                        echo Convert::ArrayToJSON(array(
                            'resultado' => true,
                            'respuesta' => (int) $resultado,
                            'residuo' => isset(explode('.', $resultado)[1]) ? (float)('.' . explode('.', $resultado)[1]) : 0,
                            'esDivisible' => (strpos($resultado, '.')) ? false : true,
                            'mensaje' => (strpos($resultado, '.')) 
                                ? 'El numero '. $primerValor .' no es divisible entre '. $segundoValor .'.'
                                : 'El numero '. $primerValor .' es divisible entre '. $segundoValor .'.'
                        ));
                    } else {
                        echo Convert::ArrayToJSON(array(
                            'resultado' => false,
                            'esDivisible' => false,
                            'mensaje' => 'No se puede dividir entre 0.'
                        ));
                    }
                } else {
                    echo Convert::ArrayToJSON(array(
                        'resultado' => false,
                        'esDivisible' => false,
                        'mensaje' => 'Alguno de los valores no ha sido especificados.'
                    ));
                }
            } else {
                echo Convert::ArrayToJSON(array(
                    'resultado' => false,
                    'esDivisible' => false,
                    'mensaje' => 'Alguno de los valores no ha sido especificados.'
                ));
            }
        }
    }
?>