var baseURL = "http://localhost";
Vue.createApp({
    data() {
        return {
            listaArticulos: Array(),
            buscarArticulo: '',
            buscarClave: '',
            nuevoArticulo: {}
        }
    }, mounted() {
        axios.get(baseURL + '/articulos')
        .then ((response)=> {
            this.listaArticulos = response.data.articulos;
        });
    }, methods: {
        onClickActualizar(articulo) {
            axios.post(baseURL + '/articulos/actualizar', {
                articulo
            }).then((response)=>{
                if (response.data.resultado) {
                    articulo.isEditing = false;

                    this.cargarArticulos();

                    $.toast({
                        heading: 'Success',
                        text: response.data.mensaje,
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                } else {
                    $.toast({
                        heading: 'Warning',
                        text: response.data.mensaje,
                        showHideTransition: 'plain',
                        icon: 'warning'
                    });
                }
            });
        },
        onClickBuscar() {
            axios.get(baseURL + '/articulos?clave='+this.buscarClave+'&articulo='+this.buscarArticulo)
            .then ((response)=> {
                this.listaArticulos = response.data.articulos;
            });
        },
        onClickRegistrar(articulo) {
            axios.post(baseURL + '/articulos/registrar', {
                articulo
            }).then((response)=>{
                if (response.data.resultado) {
                    articulo.isEditing = false;

                    this.cargarArticulos();
                    this.nuevoArticulo = {};

                    $.toast({
                        heading: 'Success',
                        text: response.data.mensaje,
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                } else {
                    $.toast({
                        heading: 'Warning',
                        text: response.data.mensaje,
                        showHideTransition: 'plain',
                        icon: 'warning'
                    });
                }
            });
        },
        onClickCancelar(articulo) {
            this.nuevoArticulo = {};
            articulo.isEditing = false;
        },
        onClickEditar(articulo) {
            articulo.isEditing = true;
        },
        onClickEliminar(articulo) {
            axios.post(baseURL + '/articulos/eliminar', {
                articulo
            }).then((response)=>{
                if (response.data.resultado) {
                    this.cargarArticulos();

                    $.toast({
                        heading: 'Success',
                        text: response.data.mensaje,
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                } else {
                    $.toast({
                        heading: 'Warning',
                        text: response.data.mensaje,
                        showHideTransition: 'plain',
                        icon: 'warning'
                    });
                }
            });
        },
        onClickNuevoRegistro() {
            this.nuevoArticulo.isEditing = true;
        },
        onKeyUpBuscar(e) {
            if (e.keyCode == 13) {
                axios.get(baseURL + '/articulos?clave='+this.buscarClave+'&articulo='+this.buscarArticulo)
                .then ((response)=> {
                    this.listaArticulos = response.data.articulos;
                });
            }
        },

        cargarArticulos() {
            axios.get(baseURL + '/articulos')
            .then ((response)=> {
                this.listaArticulos = response.data.articulos;
            });
        }
    },
}).mount(".app-crud");

Vue.createApp({
    data() {
        return {
            primerValor: '',
            segundoValor: '',
            respuesta: '',
            residuo: '',
            mensaje: '',
            esDivisible: false
        }
    },
    methods: {
        onClickCalcular() {
            axios.post(baseURL + '/dividir', {
                'primerValor': this.primerValor,
                'segundoValor': this.segundoValor
            }).then((response)=>{
                if (response.data.resultado) {
                    this.respuesta = response.data.respuesta;
                    this.residuo = response.data.residuo;
                    this.esDivisible = response.data.esDivisible;
                    this.mensaje = response.data.mensaje;
                } else {
                    this.respuesta = '';
                    this.residuo = '';
                    this.esDivisible = response.data.esDivisible;
                    this.mensaje = response.data.mensaje;
                }
            });
        }
    }
}).mount(".app-division");

Vue.createApp({
    data() {
        return {
            valor: '',
            factorial: '',
            mensaje: ''
        }
    },
    methods: {
        onKeyUpFactorial() {
            if (this.valor != '') {
                axios.post(baseURL + '/factorial', {
                    'valor': this.valor
                }).then((response)=>{
                    if (response.data.resultado) {
                        this.factorial = response.data.factorial;
                        this.mensaje = response.data.mensaje;
                    } else {
                        this.factorial = '';
                        this.mensaje = response.data.mensaje;
                    }
                });
            } else {
                this.factorial = '';
            }
        },
        getFactorialExpresion() {
            var valores = Array();

            for(i=1;i<=this.valor;i++)
                valores.push(i);

            return valores.sort(function(a, b) {
                return b - a;
            });
        }
    }
}).mount(".app-factorial")